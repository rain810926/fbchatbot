#!/usr/bin/python

from chatterbot import ChatBot
from chatterbot.training.trainers import ListTrainer


chatbot = ChatBot("Happy Bird")
chatbot.set_trainer(ListTrainer)

while True:
    Q = raw_input("Q: ")
    A = raw_input("A: ")
    for i in xrange(3):
        training_set = [Q, A]
        chatbot.train(training_set)
