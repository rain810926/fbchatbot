#!/usr/bin/python

from chatterbot import ChatBot
from chatterbot.training.trainers import ChatterBotCorpusTrainer

chatbot = ChatBot("Ron Obvious")
chatbot.set_trainer(ChatterBotCorpusTrainer)

# Train based on the english corpus
chatbot.train("chatterbot.corpus.english")

while True:
    ans = raw_input("You: ")
    response = str(chatbot.get_response(ans))
#    try:
#        response.decode('ascii')
#    except UnicodeDecodeError:
#        print "it was not a ascii-encoded unicode string"
#    else:
#        print "It may have been an ascii-encoded unicode string"
    print "Happy Bird:", response
