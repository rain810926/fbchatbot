#!/usr/bin/python

import os, csv

for root, dirs, files in os.walk("./"):
    for f in files:
        #print os.path.join(root, f)
        #print os.path.splitext(f) 
        if os.path.splitext(f)[-1] == '.txt':
            in_txt = csv.reader(open(f,"rb"), delimiter = '\t')
            out_csv = csv.writer(open(os.path.splitext(f)[0] + '.csv', 'wb'))
            out_csv.writerows(in_txt)
            print True
