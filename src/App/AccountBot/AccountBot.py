#-*- coding:utf-8 -*-
from tools.MessageModel import MessageModel
from tools.MessageParser import MessageParser
import requests, re, datetime
from pprint import pprint
from pymongo import MongoClient

class AccountBot(object):
     def __init__(self):
          self.Mongo = MongoClient('mongodb://127.0.0.1:27017')
          self.DB = self.Mongo['Account']
          self.ACCESS_TOKEN = 'EAAYRZAKAGnmABADwpHPMQk2n84xk1cN5OE8poVx9ZAJeRiBuS9yYNXLALm2aXvy6qHNHGrmQhRC26T5kXpGvxmSj9nEZAVZBNxuFOsR2s5ZAfsFVzVUzaM9hYSnv1tYjb2NNJM43DtyOZAQAI3kp5xMwtshSWT0HiORrPWKGPjAAZDZD'
          self.URL = 'https://graph.facebook.com/v2.6/me/messages?access_token='
          self.messageModel = MessageModel()
     #     self.parser(FbMessage=data)


     def parser(self, FbMessage):
          userMessage = MessageParser(FbMessage)
          print '== user input =='
          print userMessage
          print ''
          return userMessage
     #     self.handle_user_input(userMessage=userMessage)

     def dispatch(self, FbMessage):
          userMessage = self.parser(FbMessage)

          switch_type = userMessage.content['type']
          switch_value = userMessage.content['value']

          if switch_type == 'system':
               return

          elif switch_type == 'text' or switch_type == 'sticker' :
               if switch_value[0] == '$':
                    self.handle_cmd(userMessage)
                    return
               reply = self.messageModel.textModel(userMessage.sender, "你今天記帳了嗎?")

          elif switch_type == 'postback':
               self.handle_cmd(userMessage)
               return

          else:
               return


          self.send_out(reply)



     def handle_cmd(self, userMessage):
          switch_value = userMessage.content['value']
          if re.match('#Help', switch_value):
               self.Help(userMessage)

          elif re.match('#Book', switch_value):
               self.Book(userMessage)

          elif re.match('#Delete', switch_value):
               self.Delete(userMessage)

          elif re.match('\$', switch_value):
               self.Accounting(userMessage)
          # elif:
          #      pass

     def Help(self, userMessage):
          if re.match('#Help$', userMessage.content['value']):
               functions = ['即時記帳']
               payload = ['#Help account']
               buttons = [self.messageModel.quickButton(item[0], item[1]) for item in zip(functions, payload)]
               reply = self.messageModel.quickReplyModel(userMessage.sender, '你想看哪個功能的說明呢', buttons)

          elif re.match('#Help account$', userMessage.content['value']):
               document = '• 您可以輸入 "$ 款項 金額"\n  來記帳\n  範例：$ 午餐 100'
               reply = self.messageModel.textModel(userMessage.sender, document)

          else:
               return

          self.send_out(reply)

     def Accounting(self, userMessage):
          collection = self.DB[userMessage.sender]
          Today = datetime.date.today().strftime('%Y-%m-%d')
          isExist = collection.find_one({'Date': Today})

          consumption = userMessage.content['value'].split(' ')

          if len(consumption) != 3 or not(consumption[2].isnumeric()):
               warning = '您輸入的格式不對喔\n正確用法如下：\n$ 款項 金額'
               reply = self.messageModel.textModel(userMessage.sender, warning)
               self.send_out(reply)
               return

          if isExist:
               items = isExist['items']
               if items == []:
                    ID = 0
               else:
                    ID = max([item['ID'] for item in items]) + 1
               result = collection.update_one(
                    {'Date': Today},
                    {
                         "$push": {
                              "items": {
                                   "ID": ID,
                                   "itemName": consumption[1],
                                   "price": int(consumption[2])
                              }
                         }
                    }
               )
          else:
               result = collection.insert_one(
                    {
                         "Date": Today,
                         "items": [
                              {
                                   "ID": 0,
                                   "itemName": consumption[1],
                                   "price": int(consumption[2])
                              }
                         ]
                    }
               )

          data = self.messageModel.textModel(userMessage.sender, '繼續加油喔!')
          self.send_out(data)

     def Book(self, userMessage):
          def doc_parser(document):
               if document == None:
                    return []
               print document['items']
               return document['items']

          Elements = []
          Today = datetime.date.today()
          Delta = datetime.timedelta(-1)
          collection = self.DB[userMessage.sender]


          for i in xrange(5):
               title = (Today + i * Delta).strftime('%Y-%m-%d')
               account = ''
               index = 1
               totle = 0
               document = collection.find_one({'Date': title})
               items = doc_parser(document)
               for item in items:
                    totle = totle + int(item['price'])
                    account = account + str(index) + '. ' + item['itemName'] + ' ' + str(item['price']) + '\n'
                    index = index + 1

               buttons = [self.messageModel.postbackButton('刪除項目', '#Delete ' + title)]
               if account == '':
                    account = '你忘記記帳囉'
                    buttons = None

               # self.messageModel.postbackButton('新增項目', '#Add ' + title),
               Elements.append(self.messageModel.element(title, account, buttons))

          data = self.messageModel.templateModel(userMessage.sender, Elements)

          self.send_out(data)

     def Delete(self, userMessage):
          cmd = userMessage.content['value'].split(' ')
          collection = self.DB[userMessage.sender]

          if re.match('#Delete$', cmd[0]):
               buttons = []
               document = collection.find_one({'Date': cmd[1]})
               items = document['items']

               for i in xrange(len(items)):
                    buttons.append(self.messageModel.quickButton(str(i+1), '#DeleteOne '+ str(cmd[1]) + ' ' + str(i)))

               print buttons

               data = self.messageModel.quickReplyModel(userMessage.sender, '您要刪除哪一項呢?', buttons)
               self.send_out(data)
               return

          elif re.match('#DeleteOne$', cmd[0]):
               document = collection.find_one({'Date': cmd[1]})
               d = document['items'][int(cmd[2])]
               result = collection.update_one(
                    {'Date': cmd[1]},
                    {
                         "$pull": {
                              "items": d
                         }
                    }
               )
               data = self.messageModel.textModel(userMessage.sender, '刪除成功!')
               self.send_out(data)
               return

     def send_out(self, message):
         response = requests.post(self.URL + self.ACCESS_TOKEN, json=message)

         print '== server response =='
         print response.json()
         print ''
