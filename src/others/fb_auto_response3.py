#!/usr/bin/python
#-*- coding:utf-8 -*-
#ngrok http 5000

from flask import Flask, request
from pprint import pprint
import random, subprocess, os, aiml
import requests, re, json


app = Flask(__name__)
ACCESS_TOKEN = "EAAYRZAKAGnmABADwpHPMQk2n84xk1cN5OE8poVx9ZAJeRiBuS9yYNXLALm2aXvy6qHNHGrmQhRC26T5kXpGvxmSj9nEZAVZBNxuFOsR2s5ZAfsFVzVUzaM9hYSnv1tYjb2NNJM43DtyOZAQAI3kp5xMwtshSWT0HiORrPWKGPjAAZDZD"

@app.route('/', methods=['GET'])
def handle_veriication():
    return request.args['hub.challenge']

def handle_user_reply(input_pair):
    if type(input_pair[1]) == int:
        try:
            data = {"recipient": {"id": input_pair[0]}, "message": {"text": "I don't have any sticker QAQ"}}
            response = requests.post("https://graph.facebook.com/v2.6/me/messages?access_token=" + ACCESS_TOKEN, json=data)
            print "== RESPONSE =="
            print(response.content)
            print "==============\n"

        except:
            print False
    else:
        try:
            response = kernel.respond(input_pair[1])
            data = {"recipient": {"id": input_pair[0]}, "message": {"text": str(response)}}
            response = requests.post("https://graph.facebook.com/v2.6/me/messages?access_token=" + ACCESS_TOKEN, json=data)
            print "== RESPONSE =="
            print(response.content)
            print "==============\n"
        except:
            print False

def handle_user_cmd(pair):
    if re.match("#book", pair[1]):
        data = {"recipient": {"id": pair[0]},
                "message": {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "generic",
                            "elements": [{
                                "title": "rift",
                                "subtitle": "Next-generation",
                                "buttons": [{
                                    "type": "web_url",
                                    "url": "https://www.google.com",
                                    "title": "Open google"
                                    }, {
                                    "type": "postback",
                                    "title": "Call Postback",
                                    "payload": "Payload for first bubble"
                                    }]
                                }, {
                                "title": "touch",
                                "subtitle": "VR",
                                "buttons": [{
                                    "type": "web_url",
                                    "url": "https://www.google.com",
                                    "title": "Open google"
                                    }, {
                                    "type": "postback",
                                    "title": "Call Postback",
                                    "payload": "Payload for second bubble"
                                    }]

                                }]
                            }
                        }
                    }
                }
        response = requests.post("https://graph.facebook.com/v2.6/me/messages?access_token=" + ACCESS_TOKEN, json=data)
        print "== RESPONSE =="
        print(response.content)
        print "==============\n"
    elif re.match("#hello", pair[1]):
        data = {"recipient": {"id": pair[0]},
                "message": {
	            "attachment": {
		        "type": "template",
		        "payload": {
                            "template_type": "generic",
			    "elements": [{
                                "title": "嗨!我是 Happy Bird\r\n一個興趣使然的工程師\r\n有任何問題歡迎找我討論~",
                                "image_url": "http://static.blog.sina.com.tw/myimages/238/22510/images/1158402996_1.jpg",
                                "buttons": [{
                                    "type": "postback",
                                    "title": "使用說明",
                                    "payload": "#help"
                                }, {
                                    "type": "web_url",
                                    "title": "前往策略雲",
                                    "url": "http://www.simmcloud.com/"
                                }]
                            }]
                        }
                    }
                }
            }

        response = requests.post("https://graph.facebook.com/v2.6/me/messages?access_token=" + ACCESS_TOKEN, json=data)
        print "== RESPONSE =="
        print(response.content)
        print "==============\n"
    elif re.match("#help", pair[1]):
        data = {"recipient": {"id": pair[0]},
                "message": {
                    "text": "你想瞭解什麼功能呢?",
                    "quick_replies": [{
                        "content_type": "text",
                        "title": "異常偵測",
                        "payload": "#explan_detect"
                        }, {
                        "content_type": "text",
                        "title": "關鍵報告",
                        "payload": "#explan_news"
                        }, {
                        "content_type": "text",
                        "title": "財經聊聊",
                        "payload": "#explan_chat"
                        }]
                    }
                }

        response = requests.post("https://graph.facebook.com/v2.6/me/messages?access_token=" + ACCESS_TOKEN, json=data)
        print "== RESPONSE =="
        print(response.content)
        print "==============\n"

    elif re.match("#explan_detect", pair[1]):
        data = {"recipient": {"id": pair[0]},
                "message": {
                    "text": "異常量價型態與大戶進出 馬上知"
                    }
                }
        response = requests.post("https://graph.facebook.com/v2.6/me/messages?access_token=" + ACCESS_TOKEN, json=data)
        print "== RESPONSE =="
        print(response.content)
        print "==============\n"


    elif re.match("#explan_news", pair[1]):
        data = {"recipient": {"id": pair[0]},
                "message": {
                    "text": "關鍵投資消息與財經新知 不漏接"
                    }
                }
        response = requests.post("https://graph.facebook.com/v2.6/me/messages?access_token=" + ACCESS_TOKEN, json=data)
        print "== RESPONSE =="
        print(response.content)
        print "==============\n"

    elif re.match("#explan_chat", pair[1]):
        data = {"recipient": {"id": pair[0]},
                "message": {
                    "text": "財經隨意聊"
                    }
                }
        response = requests.post("https://graph.facebook.com/v2.6/me/messages?access_token=" + ACCESS_TOKEN, json=data)
        print "== RESPONSE =="
        print(response.content)
        print "==============\n"



@app.route('/', methods=['POST'])
def handle_incoming_messages():
    data = request.json
    print "== handle_incoming_messages =="
    pprint(data)
    print "==============================\n"

    if data['object'] == 'page':
        for pageEntry in data['entry']:
            pageID = pageEntry["id"]
            timeOFEvent = pageEntry["time"]
            for messagingEvent in pageEntry["messaging"]:
                if "message" in messagingEvent:
                    user_id = messagingEvent["sender"]["id"]
                    if "quick_reply" in messagingEvent["message"]:
                        action = messagingEvent["message"]["quick_reply"]["payload"]
                        handle_user_action((user_id, action))
                    elif "text" in messagingEvent["message"]:
                        msg = messagingEvent["message"]["text"]
                        handle_user_input((user_id, msg))
                    elif "sticker_id" in messagingEvent["message"]:
                        msg = messagingEvent["message"]["sticker_id"]
                        handle_user_reply((user_id, msg))

                elif "postback" in messagingEvent:
                    user_id = messagingEvent["sender"]["id"]
                    postback = messagingEvent["postback"]["payload"]
                    handle_user_action((user_id, postback))


    return 'ok'

def handle_user_input(input_pair):
    cmd_pattern = '[#+-]'
    if re.match(cmd_pattern, input_pair[1]):
        handle_user_cmd(input_pair)
    else:
        handle_user_reply(input_pair)

def handle_user_action(action_pair):
    cmd_pattern = '[#+-]'
    if re.match(cmd_pattern, action_pair[1]):
        handle_user_cmd(action_pair)

if __name__ == '__main__':
    cur_dir = os.getcwd()
    os.chdir('./alice')
    kernel = aiml.Kernel()
    kernel.learn("startup.xml")
    kernel.respond("LOAD ALICE")
    os.chdir(cur_dir)
    app.run(debug=True)
