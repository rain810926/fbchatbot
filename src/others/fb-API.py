#!/usr/bin/python
import argparse, requests, json

class fb_API(object):
    ACCESS_TOKEN = "EAAYRZAKAGnmABADwpHPMQk2n84xk1cN5OE8poVx9ZAJeRiBuS9yYNXLALm2aXvy6qHNHGrmQhRC26T5kXpGvxmSj9nEZAVZBNxuFOsR2s5ZAfsFVzVUzaM9hYSnv1tYjb2NNJM43DtyOZAQAI3kp5xMwtshSWT0HiORrPWKGPjAAZDZD"
    url = "https://graph.facebook.com/v2.6/me/thread_settings?access_token="

    def enable(self, w=False, m=False):
        if w:
            with open('Model/start.json') as f:
                welcome_msg = json.load(f)
            response = requests.post((self.url + self.ACCESS_TOKEN), json=welcome_msg)
            print response.json()
            print "Enable 'Started Button'"

        if m:
            with open('Model/menu.json') as f:
                welcome_msg = json.load(f)
            response = requests.post((self.url + self.ACCESS_TOKEN), json=welcome_msg)
            print response
            print "Enable 'Persistent Menu'"

    def disable(self, w=False, m=False):
        if w:
            remove = {"setting_type": "call_to_actions", "thread_state": "new_thread"}
            response = requests.delete((self.url + self.ACCESS_TOKEN), json=remove)
            print response
            print "Disable 'Started Button'"

        if m:
            remove = {"setting_type": "call_to_actions", "thread_state": "existing_thread"}
            response = requests.delete((self.url + self.ACCESS_TOKEN), json=remove)
            print response
            print "Disable 'Persistent Menu'"


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='You can use this command to "enable" or "disable" particular function of FB messenger bot.')
    parser.add_argument("-f", action="store_true", help="show the function list")
    parser.add_argument("-e", choices=['w','m', 'wm', 'mw'], metavar="function", help="join function you want to enable")
    parser.add_argument("-d", choices=['w','m', 'wm', 'mw'], metavar="function", help="join function you want to disable")
    args =  parser.parse_args()

    if args.e == None and args.d == None and args.f == False:
        print "Try '-h' or '--help' for more information."
        exit()

    fb = fb_API()
    if args.f:
        print 'Function list:'
        print "    'w' for 'welcome message'"
        print "    'm' for 'menu bar'"
    if args.e:
        if args.e == 'w':
            fb.enable(w=True)
        elif args.e == 'm':
            fb.enable(m=True)
        else:
            fb.enable(w=True)
            fb.enable(m=True)

    if args.d:
        if args.d == 'w':
            fb.disable(w=True)
        elif args.d == 'm':
            fb.disable(m=True)
        else:
            fb.disable(w=True)
            fb.disable(m=True)
