import argparse, os

template = "\
from tools.MessageModel import MessageModel\n\
from tools.MessageParser import MessageParser\n\
import requests\n\
class className(object):\n\
    def __init__(self, data):\n\
        self.ACCESS_TOKEN = 'EAAYRZAKAGnmABADwpHPMQk2n84xk1cN5OE8poVx9ZAJeRiBuS9yYNXLALm2aXvy6qHNHGrmQhRC26T5kXpGvxmSj9nEZAVZBNxuFOsR2s5ZAfsFVzVUzaM9hYSnv1tYjb2NNJM43DtyOZAQAI3kp5xMwtshSWT0HiORrPWKGPjAAZDZD' \n\
        self.URL = 'https://graph.facebook.com/v2.6/me/messages?access_token='\n\
        self.messageModel = MessageModel()\n\
    \n\
    def parser(self, FbMessage):\n\
        userMessage = MessageParser(FbMessage)\n\
        print '== user input =='\n\
        print userMessage\n\
        print ''\n\
        return userMessage\n\
    \n\
    def dispatch(self, FbMessage):\n\
        userMessage = self.parser(FbMessage)\n\
        '''\n\
        write your logic here\n\
        then define the handle function\n\
        '''\n\
        pass\n\
    \n\
    def send_out(self, message):\n\
        response = requests.post(self.URL + self.ACCESS_TOKEN, json=message)\n\
    \n\
        print '== server response =='\n\
        print response\n\
        print ''\n\
"

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Creat an app template')
    parser.add_argument('Apps', metavar='app name', type=str, help='python start.py fileName', nargs='+')
    args =  parser.parse_args()

    workDir = os.getcwd()

    for app in args.Apps:
        fContent = template
        fContent = fContent.replace('className', app)
        os.makedirs(workDir + '/App/' + app + '/img')
        fName = 'App/' + app + '/' + app + '.py'
        with open(fName, 'w') as App:
            App.write(fContent)
