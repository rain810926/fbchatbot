#-*- coding:utf-8 -*-
import requests

"""
button = {
    "type" : "postback",
    "title" : "title",
    "payload" : "XXX"
}
button =
    "type" : "web_url",
    "title" : "title",
    "url" : "source"
}
"""
ACCESS_TOKEN = 'EAAYRZAKAGnmABADwpHPMQk2n84xk1cN5OE8poVx9ZAJeRiBuS9yYNXLALm2aXvy6qHNHGrmQhRC26T5kXpGvxmSj9nEZAVZBNxuFOsR2s5ZAfsFVzVUzaM9hYSnv1tYjb2NNJM43DtyOZAQAI3kp5xMwtshSWT0HiORrPWKGPjAAZDZD'
URL = 'https://graph.facebook.com/v2.6/me/thread_settings?access_token='

buttons = [
    {
        "type" : "postback",
        "title" : "使用說明",
        "payload" : "#Help"
    },

    {
        "type" : "postback",
        "title" : "查看帳簿",
        "payload" : "#Book"
    }
]


data = {
    "setting_type" : "call_to_actions",
    "thread_state" : "existing_thread",
    "call_to_actions" : buttons
}

response = requests.post(URL + ACCESS_TOKEN, json=data)
print response.json()
