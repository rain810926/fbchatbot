from flask import Flask, request
from pprint import pprint
# from UserInput import UserInput
# from App.ChatBot.ChatBot import ChatBot
from App.AccountBot.AccountBot import AccountBot

app = Flask(__name__)
VERIFY_TOKEN = 'happybird'
# chatBot = ChatBot()
accountBot = AccountBot()


@app.route('/', methods=['GET'])
def webhooks_verify(): # FB webhooks authentication
    if request.args['hub.verify_token'] == VERIFY_TOKEN:
        return request.args['hub.challenge']
    else:
        return 'verify error'

# @app.route('/register/<service>', methods=['GET'])
# def register(service):
#     return service

@app.route('/', methods=['POST'])
def receive():
    print "== origin data == "
    pprint (request.json)
    print ""
    # ChatBot(request.json)
    accountBot.dispatch(request.json)
    return 'ok'



app.run(host='192.168.72.128', port=5000, debug=True)
