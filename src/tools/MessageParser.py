class MessageParser():

    def __init__(self, FbMessage):
        self.sender = FbMessage['entry'][0]['messaging'][0]['sender']['id']
        self.sendTime = FbMessage['entry'][0]['messaging'][0]['timestamp']
        self.receiveTime = FbMessage['entry'][0]['time']
        self.content = self.parser(FbMessage['entry'][0]['messaging'][0])

        """
        content = {
            'type': 'text'/'postback'/'sticker'
            'value': 'content'/'payload'/'sticker_id'
        }
        """
    def __str__(self):
        line1 = "sender id: " + self.sender + "\n"
        line2 = "send time: " + str(self.sendTime) + "\n"
        line3 = "receive time: " + str(self.receiveTime) + "\n"
        line4 = "content: " + str(self.content) + "\n"
        return line1 + line2 + line3 + line4

    def parser(self, message):
        content = {}
        content.setdefault('type', None)
        content.setdefault('value', None)
        if 'postback' in message:
            content['type'] = 'postback'
            content['value'] = message['postback']['payload']

        elif 'message' in message:
            message = message['message']
            if 'quick_reply' in message:
                content['type'] = 'postback'
                content['value'] = message['quick_reply']['payload']

            elif 'text' in message:
                content['type'] = 'text'
                content['value'] = message['text']
            else:
                content['type'] = 'sticker'
                content['value'] = message['sticker_id']
        else:
            content['type'] = 'system'

        return content
