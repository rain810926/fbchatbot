import requests


class MessageModel(object):
    content = {}
    content.setdefault('recipient', None)
    content.setdefault('message', None)
    attachType = ['image', 'video', 'audio', 'file']

    def textModel(self, recipient, text):
        message = self.content.copy()
        message['recipient'] = {'id': recipient}
        message['message'] = {'text': text}
        # response = requests.post(self.URL + self.ACCESS_TOKEN, json=content)
        return message

    def attachmentModel(self, recipient,attachType, url): #attachType: image, video, audio, file
        message = self.content.copy()
        message['recipient'] = {'id': recipient}
        attachData = {'attachment': {'type': attachType, 'payload': {'url':url}}}
        message['message'] = attachData
        return message

    def templateModel(self, recipient, elements):
        message = self.content.copy()
        message['recipient'] = {'id': recipient}

        payload = {
            'template_type': 'generic',
            'elements': elements
        }

        attachData = {
            'attachment': {'type': 'template', 'payload': payload}
        }

        message['message'] = attachData

        return message

    def element(self, title, subtitle, buttons=None, item_url=None, image_url=None):
        element = {}
        element.setdefault('title', title)
        element.setdefault('subtitle', subtitle)
        if buttons != None:
            element.setdefault('buttons', buttons)
        if item_url != None:
            element.setdefault('item_url', item_url)
        if image_url != None:
            element.setdefault('image_url', image_url)

        return element


    def quickReplyModel(self, recipient, text, buttons):
        message = self.content.copy()
        message['recipient'] = {'id': recipient}
        quickReply = {
            'text': text,
            'quick_replies': buttons
        }

        message['message'] = quickReply

        return message

    def urlButton(self, url, title, webview='full'):
        button = {
            'type': 'web_url',
            'url': url,
            'title': title,
            'webview_height_ratio': webview
        }

        return button

    def postbackButton(self, title, payload):
        button = {
            'type': 'postback',
            'title': title,
            'payload': payload
        }

        return button

    def quickButton(self, title, payload):
        button = {
            'content_type': 'text',
            'title': title,
            'payload': payload
        }

        return button
