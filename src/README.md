#Synopsis
FB chat bot
write in python, flask

#How to use
```
python src/main.py
./ngrok http 5000
```
go to your project page (fb developer)
paste the url on Webhooks

#Components
##### main.py
flask app
##### ChatBot.py
Receive the user input, and handle it
##### MessageModel.py
create the structure message used to reply
used in ChatBot
##### UserInput.py
parse the structure message sent by fb
used in ChatBot
