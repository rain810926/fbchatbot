#!/usr/bin/python
#encoding=utf-8
import jieba

jieba.set_dictionary('dict.txt')

sentence = "獨立音樂需要大家一起來推廣，歡迎加入我們的行列"
words = jieba.cut(sentence, cut_all=False)

for word in words:
    print word
